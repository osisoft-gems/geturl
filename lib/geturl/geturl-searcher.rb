require 'json'

require_relative 'geturl-file-manager'
require_relative 'geturl-local-urls'

module GetUrl

  # The Searcher deals with collecting all urls and searching through them.
  module Searcher
    extend self

    @items = []

    # Loads the urls of the source with the given id.
    # @param [String]  id   The id
    def load_source(id)
      file = FileManager.get_local_source_file(id)
      source_items = FileManager.load_items_from_yaml_file(file)
      source_items.each {|item|
        item['source'] = id
        @items << item
      }
    end

    # Loads all the given source ids. If ids is empty, all sources will be loaded.
    # @param [Array]  ids  An array of source ids.
    def load_sources(ids = nil)
      @items.clear

      all_sources = (ids.nil? || ids.empty?)
      if (all_sources) && (FileManager.all_items_cache_exist?)
        @items = FileManager.get_all_items_from_cache
        return
      end

      ids = Sources.get_sources.keys + ['local'] if ids.nil? || ids.empty?
      load_source(LocalUrls.private_source_id) if ids.include?('local')
      Sources.get_sources.keys.each {|id|
        load_source(id) if ids.include?(id)
      }
      FileManager.cache_all_items(@items) if all_sources
    end

    # Returns all urls of the given ids. If ids is empty, all urls of all sources will be loaded.
    #
    # @param [Array]  ids   The array of source ids.
    # @return [Array]       The array of url items.
    def all(ids = nil)
      load_sources(ids)
      return @items
    end

    # Searches in all urls for those that matches one or more keywords.
    # Each item gets a score on relevance.
    # For specific ids, the options['ids'] can be given.
    #
    # @param [Array]  keywords  The keywords
    # @param [Hash]   options   The options
    # @return [Array]           The array of items.
    def search(keywords = [], options = {})
      load_sources(options['ids'])

      # Calculate score for each item, for each keyword.
      @items.each {|item|
        keywords.each {|keyword|
          add_score(keyword, item)
        }
        # Not only individual keywords, but also check on all keywords combined (when mo)
        if (item.size > 1)
          full = keywords.join(' ')
          add_score(full, item, 2)
        end
      }

      # Removes all items that are not relevant (score of 0).
      @items.delete_if {|item| item['score'] == 0}

      if options['--match-all']
        @items.delete_if {|item| item['scored_keywords'] < keywords.size}
      end

      # Sort by score
      @items.sort! {|x, y|
        y['score'] <=> x['score'] rescue y <=> x
      } if (@items.size > 1)

      # Remove all score values
      @items.each {|item|
        item.delete('score')
        item.delete('scored_keywords')
      } unless options.include?('--show-score')

      n = options['-n'].to_i || 0
      @items = @items[0..n-1] if n > 0

      return @items
    end


    # Adds a score for the given keyword on the given item. The given factor gives extra weight to the score.
    #
    # @param [String]   keyword   The keyword.
    # @param [Hash]     item      The item.
    # @param [FixNum]   factor    The weighing factor.
    def add_score(keyword, item, factor = 1)
      score = 0
      begin
        score += get_field_score(item['name'], keyword, 50, 25)
        score += get_field_score(item['url'], keyword, 50, 25)
        score += get_field_score(item['description'], keyword, 30, 2)
        item['tags'].to_a.each {|tag|
          score += get_field_score(tag, keyword, 30, 20)
        }
      rescue
      end
      item['score'] ||= 0
      item['scored_keywords'] ||= 0
      if score > 0
        item['score'] += score * factor
        item['scored_keywords'] += 1
      end
    end


    # Returns the relevant score:
    # - If the given value and keyword are equal, the given equal_score is returned.
    # - Else if the given keyword is a substring of value, the contains_score is returned.
    # - Otherwise, 0 is returned.
    #
    # @param [String]  value            The value.
    # @param [String]  keyword          The keyword.
    # @param [Integer]  equal_score     The score when value is equal to the keyword.
    # @param [Integer]  contains_score  The score when value contains the keyword.
    # @return [Integer]
    def get_field_score(value, keyword, equal_score, contains_score)
      v = value.downcase
      k = keyword.downcase
      return equal_score if (v == k)
      return contains_score if v.include?(k)
      return 0
    end

  end

end
Gem::Specification.new do |gem|
  gem.name = 'geturl'
  gem.version = '0.7.2'
  gem.date = '2019-05-10'
  gem.summary = 'GetURL'
  gem.description = 'A simple command-line bookmarking tool, with support of external, read-only sources.'
  gem.authors = ['Taco Jan Osinga']
  gem.email = 'info@osingasoftware.nl'
  gem.files = `git ls-files lib`.split("\n") + %w(LICENSE)
  gem.require_paths = ['lib']
  gem.license = 'MIT'
  gem.homepage = 'https://gitlab.com/osisoft-gems/geturl'
  gem.executables = ['geturl']

  gem.add_runtime_dependency('colorize', '~> 0.0', '>= 0.8.1')
  gem.add_runtime_dependency('mustache', '~> 1.0', '>= 1.0.5')
  gem.add_runtime_dependency('cli-parser', '~> 0.0', '>= 0.0.3')
end

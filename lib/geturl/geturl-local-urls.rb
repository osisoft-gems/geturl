module GetUrl

  # The LocalUrls manages the local stored urls.
  module LocalUrls
    extend self

    @items = nil
    @private_source_id = 'local'
    @local_urls_file = File.join(Dir.home, '.geturl', 'sources', 'local.yaml')

    attr_reader :private_source_id

    # Loads all urls items from the local storage.
    def load_urls
      @items = FileManager.load_items_from_yaml_file(@local_urls_file) rescue []
    end

    # Stores all urls ites to the local storage.
    def save_urls
      FileManager.save_items_to_yaml_file(@items, @local_urls_file)
      FileManager.clear_all_items_cache
      return @items.size
    end

    # Adds or updates the data for the given url, and stores in the local storage.
    #
    # @param [String]  url     The url.
    # @param [String]  name    The name.
    # @param [Hash]  options The options, which might contains the tags and description.
    def save_url(url, name, options = {})
      return if (url.to_s.empty?) || (name.to_s.empty?)

      load_urls
      @items.delete_if {|item| item['url'] == url}
      options['tags'] ||= []
      options['tags'].map! {|tag| tag.strip}

      @items << {
          'url' => url,
          'source' => 'local',
          'name' => name,
          'description' => options['description'],
          'tags' => options['tags']
      }
      save_urls
    end

    # Deletes the given url from the local storage.
    #
    # @param [String]  url  The url.
    def delete_url(url)
      load_urls if @urls.nil?
      @items.delete_if {|item| item['url'] == url}
      save_urls
    end
  end

end
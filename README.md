# geturl

This command-line tool is a simple very basic search tool for urls, as a bookmarking system. You can add your own urls in a private file, and/or register external url-sources, which can be used as read-only lists.

## Installation
```bash
gem install geturl
```

## Usage example

### Searching
The main purpose of _geturl_ is to search specific urls (items).

```bash
geturl keyword1 keyword2 ...
```

Each known item is checked and given a score for relevance.
A score is calculated based on the name, the url, the description and the tags. 
The item with the highest score is the first in the resulting output.
In above example all items that either contain keyword1 or keyword2 are returned.

```bash
geturl --match-all keyword1 keyword2 ...
geturl -a keyword1 keyword2 ...
```

In above examples only the items that contain both keyword1 and keyword2 are returned.
Both `--match-all` and `-a` are the same. 

It is possible to limit the results, using the `-n` option: 
```bash
geturl -n 2 keyword1 keyword2
```

To get all available items, use `--show-url`:
```bash
geturl --show-all
```

To only output the found urls, use:
```bash
geturl --only-urls keyword1 keyword2 ...
geturl -u keyword1 keyword2 ...
```

### Formatting

#### Text
By default all items are output in text:
```bash
$> geturl example
```
```
Example website
https://www.example.com
This is an example website.
Tags: test1, test2, example1, example2
```

#### JSON
It is possible to use different formatting, like json and yaml:
```bash
$> geturl example --format json
```
```json
[
  {
    "url": "https://www.example.com",
    "name": "Example website",
    "description": "This is an example website.",
    "tags": [
      "test1",
      "test2",
      "example1",
      "example2"
    ],
    "source": "local"
  }
]
```

#### Yaml
```bash
$> geturl example --format yaml
```

```yaml 
- url: https://www.example.com
  name: Example website
  description: This is an example website.
  tags:
  - test1
  - test2
  - example1
  - example2
  source: local
```

#### Custom output with Mustache templates

For more complex output, you can use Mustache to create whatever you need.
The template can be given as a string, or as a file. 

```bash
geturl example --format mustache --template "{{#items}}{{name}}<br />{{/items}}"
```

```html
Example website<br />
```

The option `--show-urls` or short `-u` is actually a shorthand for
`--format mustache --template "{{#items}}{{url}}\n{{/items}}"`.

The following example demonstrates the usage of a mustache template that create items for the macOS application called Alfred.
```bash
geturl {query} --format mustache --template '<?xml version="1.0" encoding="utf-8"?><items>{{#items}}<item valid="yes"><title>{{name}}</title><subtitle>{{description}}</subtitle><arg>{{url}}</arg><text type="copy">{{url}}</text></item>{{/items}}</items>'
```

It is also possible to move the xml in the example above to a seperate file, and reference that file with an @ sign.
```bash
geturl example --format mustache --template @xml-for-alfred-app.mustache
```

```xml
<?xml version="1.0" encoding="utf-8"?>
<items>
      <item valid="yes">
        <title>Example website</title>
        <subtitle>This is an example website.</subtitle>
        <arg>https://www.example.com</arg>
        <text type="copy">https://www.example.com</text>
      </item>
</items>
```


### Storing urls locally
It is possible to add bookmarks to the local storage (stored in `./geturls/sources/local.yaml`). 
```bash
geturl --save https://www.example.com "Example Website"
```

It is also possible to add a description, and comma separated list of tags.
```bash
geturl --save https://www.example.com "Example website" --description "This is an example website." --tags test1,test2,example1,example2
```

When you want to delete an item
```bash
geturl --delete http://www.example.com
```

Want to do massive updates? All local urls are stored in: `~/.geturl/sources/local.yaml`, which is fairly to update yourself.

### Read-only bookmarks from external sources

One of the main reasons to create _geturl_ is to have urls stored somewhere on the internet as url source.
You can register multiple sources, after which the external data is cached locally. 

To list all registered sources:
```bash
geturl --sources
```

To reload all registered sources:
```bash
geturl --reload
```

It's also possible to reload a specific source, by adding the source id. 
```bash
geturl --reload example-id
```

To register a new source, register the external url with any id:
```bash
geturl --register example-id http://www.example.com/example-list.yaml
```

To search within a specific source:
```bash
geturl --source example-id keyword1 keyword2
```

To list all available items from a specific source:
```bash
geturl --show-all example-id
```

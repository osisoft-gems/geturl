require 'yaml'
require 'fileutils'
require 'open-uri'

require_relative 'geturl-file-manager'

module GetUrl

  # The Sources represents the external, read-only sources.
  module Sources
    extend self

    # Ensure the .geturl folder exists.
    @sources_path = File.join(Dir.home, '.geturl')
    FileUtils.mkdir_p(@sources_path)

    # Ensure the sources.yaml file exists
    @source_file = File.join(Dir.home, '.geturl', 'sources.yaml')
    FileUtils.touch(@source_file) unless File.exist?(@source_file)

    # Returns all the sources as a two dimensional Hash, with the id as key, and futher information as a child Hash.
    #
    # @return [Hash] The sources.
    def get_sources
      data = File.read(@source_file)
      YAML.load(data).to_h rescue {}
    end

    # Registers the given sources. If the id already exists, it will be overwritten without warning.
    #
    # @param [String]  id     The id of the source.
    # @param [String]  url    The url of the source.
    # @param [Hash]  options  The options.
    # @return [Integer]       The size of the sources.
    def register_source(id, url, options = {})
      id.downcase!
      raise ArgumentError.new('The source id \'local\' is reserved.') if (id == 'local')
      sources = get_sources || {}
      sources[id] = {
          'url' => url,
          'cert' => options['cert'],
          'key' => options['key'],
      }
      File.write(@source_file, sources.to_yaml)
      FileManager.clear_all_items_cache
      return sources.size
    end

    # Unregisters the source with the given id. It also removes the locally cached file.
    #
    # @param [String]  id     The id of the source.
    # @return [Integer]       The size of the sources.
    def unregister_source(id)
      sources = get_sources || {}
      sources.delete(id)
      File.write(@source_file, sources.to_yaml)
      File.unlink(FileManager.get_local_source_file(id)) rescue nil
      FileManager.clear_all_items_cache
      return sources.size
    end

    # Reloads the yaml file from the source, and caches it locally.
    # If the id is nil, all sources will be reloaded.
    #
    # @param [Object]  id       The id of the source to reload.
    # @param [Object]  options  The options.
    def reload(id = nil, options = {})
      if id.nil?
        get_sources.keys.each {|i|
          reload(i, options) unless i.nil?
        }
      else
        begin
          source_data = get_sources[id]
          print "Loading #{id} ...\r" if (options[:verbose])

          if source_data.include?('cert') && source_data.include?('key')
            curl_command = "curl -s #{source_data['url']} -E #{source_data['cert']} --key #{source_data['key']}"
            data = `#{curl_command}`
          else
            data = open(source_data['url']).read
          end
          raise Exception.new('File is empty') if data.empty?
          file = FileManager.get_local_source_file(id)
          FileManager.clean_and_save_items_to_yaml_file(data, file)
          puts "Loaded #{id}" + "".ljust(5) if (options[:verbose])
        rescue Exception => e
          puts "Failed to load #{id}: #{e.full_message}" if (options[:verbose])
        end
      end
    end

  end

end
